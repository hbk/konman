use std::path::Path;
use std::path::PathBuf;
use std::fs;
use config;

pub fn help() {
    println!("{}", config::HELP_MESSAGE);
}

pub fn location(konman_path: &PathBuf) {
    let location = konman_path.as_path().display();
    println!("Location of dotfiles: {}", location);
}

pub fn list(konman_path: &PathBuf) {
    traverse_konman(konman_path.as_path(), 1);
}

fn traverse_konman (current_dir: &Path, indent: u8) {
    let entries = fs::read_dir(current_dir).unwrap();
    for entry in entries {
        let path = entry.unwrap().path();
        if path.is_dir() {
            let dir_name = path.file_name().unwrap();
            let dashes = make_dashes(indent);
            println!("{} Contents of: {}", dashes, dir_name.to_str().unwrap());
            traverse_konman(&path, indent + 1);
        } else {
            println!("{} {}", make_dashes(indent - 1), path.display());
        }
    }
}

fn make_dashes(num: u8) -> String {
    let mut dashes = String::new();
    for i in 0..num {
        dashes.push_str("-");
    }
    dashes
}

pub fn restore_all() {
    println!("Copies or links all files from konman back to system.");
}

pub fn remove_all() {
    println!("Removes all files from konman.");
}

pub fn add(files: &[String], konman_path: &Path) {
    for file in files {
        let file_path = Path::new(&file);
        match file_path.canonicalize() {
            Ok(path) => add_file(&path, &konman_path),
            Err(msg) => println!("Error for file \"{}\": {}", &file, msg),
        }
    }
}

fn add_file(file: &Path, konman_path: &Path) {
    let parent_path = file.parent().unwrap();
    let parent = String::from(parent_path.to_str().unwrap());
    let mut link_path = PathBuf::from(konman_path.join(&parent[1..]));
    fs::create_dir_all(&link_path).unwrap();
    link_path.push(&file.file_name().unwrap());
    match fs::hard_link(&file, &link_path) {
        Ok(_) => println!("Added file \"{}\" to konman", file.display()),
        Err(_) => println!("Error: \"{}\" already exists in konman", file.display()),
    }
}

pub fn remove(files: &[String]) {
    println!("Remove given file(s) from konman");
    println!("It works in two ways:");
    println!("1- Take a file name and search konman for that file.");
    println!("2- Take relative or full path of a file and search that in konman");
    println!("If a file is found remove it. If the dir is empty remove that also. ");
}

pub fn restore(files: &[String]) {
    println!("Restore given file(s) from konman back to system");
    println!("Should file detection be with paths or just file names?");
}

