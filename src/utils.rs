use commands;
use std::fs;
use std::io;
use std::env;
use std::fs::File;
use std::io::Write;
use std::path::PathBuf;
use std::path::Path;

static CREATE_DIR_ERROR: &'static str = "Permission required or directory already exists";

pub fn run_command(command: &str, konman_path: PathBuf) {
    match command {
        "help" => commands::help(),
        "list" => commands::list(&konman_path),
        "location" => commands::location(&konman_path),
        "restore-all" => commands::restore_all(),
        "remove-all" => commands::remove_all(),
        _ => commands::help(),
    }
}

pub fn run_command_files(command: &str, files: &[String], konman_path: PathBuf) {
    match command {
        "add" => commands::add(files, konman_path.as_path()),
        "remove" => commands::remove(files),
        "restore" => commands::restore(files),
        _ => commands::help(),
    }
}

pub fn get_config_path() -> PathBuf {
    let home_path = env::home_dir().unwrap();
    let mut conf_path = PathBuf::from(home_path);
    conf_path.push(".config");
    conf_path.push("konman");
    conf_path.push("config");
    conf_path
}

pub fn init() -> String {
    let conf = get_config_path();
    let conf_path = conf.as_path();
    let conf_dir = conf_path.parent().unwrap();
    
    println!("Select a location [Press enter for default at \"~/.dotfiles\"]: ");
    let mut user_dir = String::new();
    io::stdin()
      .read_line(&mut user_dir)
      .expect("Failed to read the location");

    if user_dir.eq("\n") {
        let mut home_path = PathBuf::from(env::home_dir().unwrap());
        home_path.push(".dotfiles");
        let konman_path = home_path.as_path();
        fs::create_dir_all(konman_path)
            .expect(&CREATE_DIR_ERROR);
        fs::create_dir_all(conf_dir)
            .expect(&CREATE_DIR_ERROR);
        let mut conf_file = File::create(conf_path).unwrap();
        let path_str = konman_path.as_os_str().to_str().unwrap();
        conf_file.write(path_str.as_bytes()).unwrap();
        String::from(konman_path.to_str().unwrap())
    } else {
        let user_path = Path::new(&user_dir);
        fs::create_dir(user_path).unwrap();
        user_dir.clone()
    }
}
 
