pub static HELP_MESSAGE: &'static str = "Konman - Configuration Manager
Usage:
konman <command> <files>
You can omit <files> if you use \"help\" or \"restore-all\" or \"list\" commands.

Commands:
    help: Get to this help text
    add: Add files to konman
    remove: Remove files from konman
    list: List added files to konman

Examples:
- Add files in current working directory
konman add .vimrc

- Add multiple files
konman add .bashrc .bash_profile
konman add *.config

- Add with full path
konman add ~/.config/alacritty/alacritty.yml

- Add directories and everything in it
konman add ~/.config/i3

- Remove files via its original path or just its name.
konman remove compton
konman remove ~/.config/i3/config";
