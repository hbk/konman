extern crate konman;
use std::fs::File;
use std::env;
use std::io::Read;
use std::path::PathBuf;
use konman::utils;
use konman::commands;

fn main() {
    let args: Vec<String> = env::args().collect();
    let mut konman_dir = String::new();
    let config_path = utils::get_config_path();
    let config_file = File::open(config_path.as_path());
    if config_file.is_ok() {
        config_file.unwrap().read_to_string(&mut konman_dir);
    } else {
        konman_dir = utils::init();
    }

    let konman_path = PathBuf::from(&konman_dir);

    if args.len() == 2 {
        utils::run_command(&args[1], konman_path);
    } else if args.len() >= 3 {
        utils::run_command_files(&args[1], &args[2..], konman_path);
    } else {
        commands::help();
    }
}
